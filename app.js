const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const app = express();

const puppeteer = require('puppeteer');

const PORT = process.env.PORT || 4000;

app.use(bodyParser.json());

let chart1Data = {};
let chart2Data = {};
let chart3Data = {};

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
//setup public folder
app.use(express.static('./public'));

// View Chart 1
app.get('/chart/1', function (request, response) {
  response.render('chart1', {
    ...chart1Data,
  });
});

// View Chart 2
app.get('/chart/2', function (request, response) {
  response.render('chart2', {
    ...chart2Data,
  });
});

// View Chart 3
app.get('/chart/3', function (request, response) {
  response.render('chart3', {
    ...chart3Data,
  });
});

// Get chart1 Image
app.post('/chart1', async (request, response) => {
  const { title, xAxisTitle, yAxisTitle, data } = request.body;
  if (!title || !xAxisTitle || !yAxisTitle || !data) {
    return response.status(400).json({ message: 'Bad Resuest!' });
  }
  chart1Data = {
    title,
    xAxisTitle,
    yAxisTitle,
    data,
  };
  const browser = await puppeteer.launch({
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
    headless: true,
    timeout: 10000,
  });
  const page = await browser.newPage();
  await page.setViewport({
    width: 450,
    height: 200,
    deviceScaleFactor: 1,
  });
  await page.goto('http://localhost:4000/chart/1', {
    waitUntil: 'networkidle2',
  });
  // await page.waitForRequest('http://104.197.192.5/getData');
  await page.waitForSelector('span#new');
  await page
    .screenshot({
      path: 'chart1.png',
      omitBackground: false,
      type: 'png',
      fullPage: true,
    })
    .catch((error) => console.log('screenshot error', error));
  await page.close();
  await browser
    .close()
    .catch((error) => console.log('browser close error', error));
  const localFilePath = `${__dirname}/chart1.png`;
  return response.download(localFilePath);
});

// Get chart3 Image
app.post('/chart3', async (request, response) => {
  const { title, xAxisTitle, yAxisTitle, data } = request.body;
  if (!title || !xAxisTitle || !yAxisTitle || !data) {
    return response.status(400).json({ message: 'Bad Resuest!' });
  }
  chart3Data = {
    title,
    xAxisTitle,
    yAxisTitle,
    data,
  };
  const browser = await puppeteer.launch({
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
    headless: true,
    timeout: 10000,
  });
  const page = await browser.newPage();
  await page.setViewport({
    width: 450,
    height: 200,
    deviceScaleFactor: 1,
  });
  await page.goto('http://localhost:4000/chart/3', {
    waitUntil: 'networkidle2',
  });
  // await page.waitForRequest('http://104.197.192.5/getData');
  await page.waitForSelector('span#new');
  await page
    .screenshot({
      path: 'chart3.png',
      omitBackground: false,
      type: 'png',
      fullPage: true,
    })
    .catch((error) => console.log('screenshot error', error));
  await page.close();
  await browser
    .close()
    .catch((error) => console.log('browser close error', error));
  const localFilePath = `${__dirname}/chart3.png`;
  return response.download(localFilePath);
});

// Get chart2 Image
app.post('/chart2', async (request, response) => {
  const { labels, label, colors, data } = request.body;
  if (!labels || !label || !data) {
    return response.status(400).json({ message: 'Bad Resuest!' });
  }
  let stringData = '[';
  let dataset = [];
  for (let i = 0; i < data.length; i++) {
    const d = data[i];
    const color = colors[i];
    let da = {
      label: '',
      // backgroundColor: `rgba(${color}, 0.2)`,
      backgroundColor: `rgba(0, 0, 0, 0)`,
      borderColor: color,
      data: d,
    };
    // if (i !== data.length - 1) {
    //   stringData = stringData + JSON.stringify(da) + ', ';
    // } else {
    //   stringData = stringData + JSON.stringify(da);
    // }
    dataset.push(da);
  }

  // let string
  chart2Data = {
    labels,
    label,
    data: dataset,
  };
  const browser = await puppeteer.launch({
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
    headless: true,
    timeout: 10000,
  });
  const page = await browser.newPage();
  await page.setViewport({
    width: 805,
    height: 800,
    deviceScaleFactor: 1,
  });
  await page.goto('http://localhost:4000/chart/2', {
    waitUntil: 'networkidle2',
  });
  // await page.waitForSelector('span#new');
  await page
    .screenshot({
      path: 'chart2.png',
      omitBackground: false,
      type: 'png',
      fullPage: true,
    })
    .catch((error) => console.log('screenshot error', error));
  await page.close();
  await browser
    .close()
    .catch((error) => console.log('browser close error', error));
  const localFilePath = `${__dirname}/chart2.png`;
  return response.download(localFilePath);
});

app.listen(PORT, console.log(`Server started on Port: ${PORT}`));
